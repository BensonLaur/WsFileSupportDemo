// dllTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include <memory>
#include <functional>
using namespace std;

#include "..\MprSupport\IFileSupport.h"

//�˳���ͣ��ʾ
void pressToExit()
{
	cout << endl << "press any key to exit ...";
	getchar();
}

//��ȡ GetLastError �����ַ�������
void win32perror(const TCHAR *tip)
{
	LPVOID buf;
	if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_IGNORE_INSERTS
		| FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_MAX_WIDTH_MASK,
		NULL,
		GetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&buf,
		0,
		NULL))
	{
		wstring wstrTopA((LPTSTR)tip);
		char strTipA[1024];
		int nSize = WideCharToMultiByte(CP_ACP, 0, (LPTSTR)tip, wstrTopA.size(), strTipA, 1024, 0, NULL);
		strTipA[nSize] = '\0';

		wstring wstrbuf((LPTSTR)buf);
		char strBufA[1024];
		nSize = WideCharToMultiByte(CP_ACP, 0, (LPTSTR)buf, wstrbuf.size(), strBufA, 1024, 0, NULL);
		strBufA[nSize] = '\0';

		//_tprintf(L"%s:%s\n", s, buf);
		//wcout << s << L":" << buf << endl;

		cout << strTipA << ":" << strBufA << endl;

		fflush(stderr);
		LocalFree(buf);
	}
	else
		wcout << tip << L"unknown Windows error" << endl;
}

//���ò���
int testInvoke()
{
	//�����Ż�DLL
	HMODULE handle = LoadLibrary(_T("MprSupport.dll"));
	if (NULL == handle)
	{
		win32perror(L"����dllʧ��");
		pressToExit();
		return -1;
	}

	std::shared_ptr<HINSTANCE__>pHandle (handle, FreeLibrary);

	_GetFileSupport funcGetFileSupport = (_GetFileSupport)GetProcAddress(handle, "GetFileSupport");   //���Һ�����ַ
	if (funcGetFileSupport == NULL)
	{
		win32perror(L"GetProcAddress ���� funcGetFileSupport ʧ��");
		pressToExit();
		return -1;
	}

	std::shared_ptr<IFileSupport> pFileSupport(funcGetFileSupport(), std::mem_fn(&IFileSupport::Deleter));

	wstring fileType(pFileSupport->GetFileTypeName());

	wcout << fileType.c_str() << endl;
	cout << pFileSupport->IsSupportPgiToFile() << endl;
	cout << pFileSupport->FileToPgi(nullptr, nullptr) << endl;
	cout << pFileSupport->PgiToFile(nullptr, nullptr) << endl;
	cout << pFileSupport->CheckAccessRight() << endl;

	return 0;
}

//�������
int main()
{
	if(-1 == testInvoke())
		return -1;
	
	pressToExit();

    return 0;
}

