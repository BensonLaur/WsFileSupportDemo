#include "IFileSupport.h"
#include <iostream>
using namespace std;

class MprFileSupport :public IFileSupport
{

public:
	/*
	*	@function					获得文件类型名
	*   @param	wszFileName：		本模块支持的文件类型名
	*   @param	nBufferSize：		wszFileName 缓存大小
	*	@return						wszFileTypeName 中以unicode 编码返回文件名
	*   @note
	*/
	virtual const wchar_t* GetFileTypeName()
	{
		return L"MPR";
	}

	/*
	*	@function					是否支持 由 pgi 文件转为 本模块类型 文件
	*	@return						如果不支持返回0，支持返回非零
	*   @note
	*/
	virtual int IsSupportPgiToFile()
	{
		return 1;
	}

	/**
	*	@function					本模块类型 文件 转换为PGI 文件
	*   @param	wszFilePath：		本模块类型 文件路径
	*   @param	wszPgiPath：			PGI 文件路径
	*	@return						如果成功返回0，否则返回非零(错误代码)
	*   @note
	**/
	virtual int FileToPgi(const wchar_t* wszFilePath, const wchar_t* wszPgiPath)
	{
		return 0;
	}


	/**
	*	@function					PGI 文件 转换为 本项目支持文件
	*   @param	wszPgiPath：			PGI 文件路径
	*   @param	wszFilePath：		本项目支持文件路径
	*	@return						如果成功返回0，否则返回非零(错误代码)
	*   @note
	**/
	virtual int PgiToFile(const wchar_t* wszPgiPath, const wchar_t* wszFilePath)
	{
		return 0;
	}



	/**
	*	@function					验证使用权限
	*	@return						如果有权限返回0，否则返回非零(错误代码)
	*   @note
	**/
	virtual int CheckAccessRight()
	{
		return 1;
	}

	/**
	*	@function					释放自己的的内存
	*   @note
	**/
	virtual void Deleter()
	{
		delete this;
		cout << "detelter invoked" << endl;
	}

};

extern "C"
{
	FILE_SUPPORT_API IFileSupport* GetFileSupport()
	{
		return new MprFileSupport();
	}
}
