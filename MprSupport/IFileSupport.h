//////////////////////////////////////////////////////////////////////////////////////////////////////////
//广州王石软件在部分软件上提供一种自定义文件解析和生成机制，该机制允许插件式地增长对不同类型板块描述文件的支持。
//	通过约定统一数据结构 以及统一的文件解析和文件生成等接口，以实现插件式添加，添加一种文件支持，只需要在对应目
//	录放置相应的dll即可
//

/*
* 本头文件可以在本项目使用，也可在接入本项目dll的项目中引用
* 本项目预编译项中，预定义了  WS_FILE_SUPPORT_EXPORTS，这样，本项目这里定义的接口（FILE_SUPPORT_API）将被定义为导出
* 任何其他使用本 dll 的项目上不用定义WS_FILE_SUPPORT_EXPORTS，这样，其他项目引用本头文件将视 FILE_SUPPORT_API 为导入
*/
#ifdef WS_FILE_SUPPORT_EXPORTS
#define FILE_SUPPORT_API __declspec(dllexport)
#else
#define FILE_SUPPORT_API __declspec(dllimport)
#endif

/*
*	文件支持接口
*/
class FILE_SUPPORT_API IFileSupport
{
public:
	/*
	*	@function					获得文件类型名
	*   @param	wszFileName：		本模块支持的文件类型名
	*   @param	nBufferSize：		wszFileName 缓存大小
	*	@return						wszFileTypeName 中以unicode 编码返回文件名
	*   @note
	*/
	virtual const wchar_t* GetFileTypeName() = 0;

	/*
	*	@function					是否支持 由 pgi 文件转为 本模块类型 文件
	*	@return						如果不支持返回0，支持返回非零
	*   @note
	*/
	virtual int IsSupportPgiToFile() = 0;

	/**
	*	@function					本模块类型 文件 转换为PGI 文件
	*   @param	wszFilePath：		本模块类型 文件路径
	*   @param	wszPgiPath：			PGI 文件路径
	*	@return						如果成功返回0，否则返回非零(错误代码)
	*   @note
	**/
	virtual int FileToPgi(const wchar_t* wszFilePath, const wchar_t* wszPgiPath) = 0;

	/**
	*	@function					PGI 文件 转换为 本项目支持文件
	*   @param	wszPgiPath：			PGI 文件路径
	*   @param	wszFilePath：		本项目支持文件路径
	*	@return						如果成功返回0，否则返回非零(错误代码)
	*   @note
	**/
	virtual int PgiToFile(const wchar_t* wszPgiPath, const wchar_t* wszFilePath) = 0;


	/**
	*	@function					验证使用权限
	*	@return						如果有权限返回0，否则返回非零(错误代码)
	*   @note
	**/
	virtual int CheckAccessRight() = 0;

	/**
	*	@function					释放自己的的内存
	*   @note						delete 的作用是用于外界使用的，由于外部通过 GetFileSupport 由本模块在堆中分配内存
	*								释放也在本模块释放，这里提供 Deleter 供外界调用。
	*	外界调用的形式可以是：
	*
	*	HMODULE handle = LoadLibrary(_T("MprSupport.dll"));					//加载DLL
	*
	*	if (NULL == handle){cout << GetLastError() << endl;return -1;}		//加载失败退出
	*
	*	std::shared_ptr<HINSTANCE__>pHandle (handle, FreeLibrary);			//句柄交给智能指针处理
	*
	*	_GetFileSupport funcGetFileSupport = (_GetFileSupport)GetProcAddress(handle, "GetFileSupport");   //查找函数地址
	*
	*	if (funcGetFileSupport == NULL){cout << GetLastError() << endl;return -1;}	//找不到函数退出
	*
	*	//获得文件支持对象指针，并交由智能指针处理（自动由 shared_ptr 的构造函数中的 Deleter 负责释放内存）
	*	std::shared_ptr<IFileSupport> pFileSupport(funcGetFileSupport(), std::mem_fn(&IFileSupport::Deleter));
	*
	*	一个简单地 Deleter 实现可以是：
	*
	*	void Deleter()
	*	{
	*		delete this;
	*	}
	**/
	virtual void Deleter() = 0;
};


//声明函数指针，告诉程序从动态链接库导入的地址是什么类型的函数
typedef IFileSupport*(*_GetFileSupport)();


/*

开发新文件支持说明，以 Mpr 文件支持为例，写一个 Mpr支持类 MprFileSupport 继承 IFileSupport
然后，以 C 编译器方式，导出 FILE_SUPPORT_API IFileSupport* GetFileSupport() 接口即可

#include "IFileSupport.h"

class MprFileSupport :public IFileSupport
{
public:
	......
};

extern "C"
{
	FILE_SUPPORT_API IFileSupport* GetFileSupport()
	{
		return new MprFileSupport();
	}
}


*/


